# ...

Example showing:
- REST server
-


## Notes on components


## devenv

```
docker-compose run --rm devenv mvn test
```


## Maven

```bash
mvn clean
mvn test
mvn package
mvn install

mvn exec:java

mvn help:describe -Dplugin=versions -Dfull
mvn versions:display-dependency-updates
mvn versions:display-plugin-updates
mvn versions:update-child-modules
mvn versions:use-latest-versions
```


```bash
mvn archetype:generate -DarchetypeArtifactId=jersey-quickstart-grizzly2 \
-DarchetypeGroupId=org.glassfish.jersey.archetypes -DinteractiveMode=false \
-DgroupId=com.example -DartifactId=simple-service -Dpackage=com.example \
-DarchetypeVersion=2.35
```

```
https://github.com/eclipse-ee4j/jersey/tree/master/examples/https-clientserver-grizzly
https://github.com/eclipse-ee4j/jersey/tree/3.0.3/examples/https-clientserver-grizzly
https://search.maven.org/artifact/org.glassfish.jersey/jersey-bom/3.0.3/pom
https://github.com/eclipse-ee4j/jersey/tree/master/archetypes/jersey-quickstart-grizzly2
```

## Libraries

### cfg4j

- http://www.cfg4j.org/
- https://javadoc.io/doc/org.cfg4j/cfg4j-core/latest/index.html

### ormlite

- https://ormlite.com/
- https://ormlite.com/javadoc/ormlite-core/doc-files/ormlite.html
  - https://ormlite.com/javadoc/ormlite-core/doc-files/ormlite.html#Logging
- https://ormlite.com/javadoc/ormlite-core/
- https://ormlite.com/javadoc/ormlite-jdbc/
