package com.gitlab.aucampia.eg.db;

import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.Table;

import com.gitlab.aucampia.eg.core.ShoppingList;
import com.gitlab.aucampia.eg.core.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Testcontainers(disabledWithoutDocker = true)
public class UserIntegrationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserIntegrationTests.class);

    // {
    // LOGGER.error("entry ...");
    // }

    @Container
    private static final PostgreSQLContainer<?> PGSQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse("postgres:13")).withUsername("pgtest").withPassword("ieShei8eivoR2geT")
                    .withReuse(true);

    private static JdbcConnectionSource connectionSource = null;
    private static Dao<User, Integer> userDao = null;
    private static Dao<ShoppingList, Integer> shoppingListDao = null;

    @BeforeAll
    public static void setup() throws Exception {
        assertTrue(PGSQL_CONTAINER.isRunning());
        LOGGER.info("PGSQL_CONTAINER.getJdbcUrl() = {}", PGSQL_CONTAINER.getJdbcUrl());
        connectionSource = new JdbcConnectionSource(PGSQL_CONTAINER.getJdbcUrl(), PGSQL_CONTAINER.getUsername(),
                PGSQL_CONTAINER.getPassword());
        userDao = DaoManager.createDao(connectionSource, User.class);
        shoppingListDao = DaoManager.createDao(connectionSource, ShoppingList.class);

        TableUtils.createTableIfNotExists(connectionSource, User.class);
        TableUtils.createTableIfNotExists(connectionSource, ShoppingList.class);
    }

    @BeforeEach
    void init() throws Exception {
        TableUtils.clearTable(connectionSource, User.class);
    }

    @Test
    void something() throws Exception {
        User user1 = new User("John Doe");
        userDao.create(user1);

        ShoppingList shoppingList1 = new ShoppingList(user1, "SL1");
        shoppingListDao.create(shoppingList1);
        ShoppingList shoppingList2 = new ShoppingList(user1, "SL2");
        shoppingListDao.create(shoppingList2);

        userDao.refresh(user1);
        assertEquals(user1.getShoppingLists().size(), 2);

    }
}
