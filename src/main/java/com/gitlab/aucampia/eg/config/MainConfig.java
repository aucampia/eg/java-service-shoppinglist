package com.gitlab.aucampia.eg.config;

import java.net.URL;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
public class MainConfig {
    @NonNull
    private String hostName = "127.0.0.1";
    @NonNull
    private Integer port = 58223;
    @NonNull
    private String prefix = "/";

    public URL getBaseURL() throws Exception {
        return new URL("http", this.getHostName(), this.getPort(), this.getPrefix());
    }
}
