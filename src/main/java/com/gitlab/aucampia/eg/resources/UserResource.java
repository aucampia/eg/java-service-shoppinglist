package com.gitlab.aucampia.eg.resources;

import com.gitlab.aucampia.eg.core.ShoppingList;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/user")
public class UserResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent to the
     * client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("/shopping_lists")
    public List<ShoppingList> getShoppingLists() {
        return List.of();
    }
}
